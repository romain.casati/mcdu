const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const buildPath = path.resolve(__dirname, "build");

module.exports = {
  entry: "./src/main.tsx",
  output: {
    filename: "[contenthash].js",
    chunkFilename: "[contenthash].js",
    path: buildPath,
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(js)x?$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.(ts)x?$/,
        exclude: /node_modules/,
        use: ["ts-loader"],
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: "asset/resource",
      },
      {
        resourceQuery: /raw/,
        type: "asset/source",
      },
    ],
  },
  resolve: {
    extensions: ["*", ".ts", ".tsx", ".js", ".jsx"],
  },
  plugins: [
    new HtmlWebpackPlugin({ title: "Mcdu" }),
    new MiniCssExtractPlugin({ filename: "[contenthash].css" }),
    new CopyPlugin({
      patterns: [
        {
          from: "./src/.htaccess",
          to: buildPath,
        },
      ],
    }),
  ],
  devServer: {
    static: {
      directory: buildPath,
    },
    port: 3000,
  },
};
