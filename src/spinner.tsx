import styles from "./styles.module.scss";

const Spinner = () => {
  return <span className={styles.spinner} />;
};

export { Spinner };
