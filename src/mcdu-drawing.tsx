import { Grid } from "./grid";
import styles from "./styles.module.scss";

interface MCDUDrawingProps {
  number: number;
  useM: boolean;
  useC: boolean;
  useD: boolean;
}

const repeat = (ntimes: number, func: (_: number) => any) =>
  [...Array(ntimes)].map((_, key) => func(key));

const MCDUDrawing = ({ number, useM, useC, useD }: MCDUDrawingProps) => {
  const use = [useM, useC, useD, true];
  const nbrs = [];
  for (let i = 0; i < 4; i++) {
    const pow = Math.pow(10, 3 - i);
    const nbr = use[i] ? Math.floor(number / pow) : 0;
    nbrs.push(nbr);
    number -= nbr * pow;
  }

  return (
    <div className={styles["mcdu-drawing"]}>
      <div className={styles["mcdu-draw-container"]}>
        {repeat(nbrs[0], (key) => (
          <Grid
            size={[10, 10, 10]}
            className={styles["mcdu-draw-m"]}
            key={key}
          />
        ))}
      </div>
      <div className={styles["mcdu-draw-container"]}>
        {repeat(nbrs[1], (key) => (
          <Grid size={[10, 10]} className={styles["mcdu-draw-c"]} key={key} />
        ))}
      </div>
      <div className={styles["mcdu-draw-container"]}>
        {repeat(nbrs[2], (key) => (
          <Grid size={10} className={styles["mcdu-draw-d"]} key={key} />
        ))}
      </div>
      <div className={styles["mcdu-draw-container"]}>
        {repeat(nbrs[3], (key) => (
          <Grid size={1} className={styles["mcdu-draw-u"]} key={key} />
        ))}
      </div>
    </div>
  );
};

export { MCDUDrawing };
