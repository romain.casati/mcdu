import styles from "./styles.module.scss";
import { ChangeEventHandler } from "react";

interface MCDUSelectorProps extends React.HTMLAttributes<HTMLDivElement> {
  useM: boolean;
  useC: boolean;
  useD: boolean;
  onChangeUseM: ChangeEventHandler;
  onChangeUseC: ChangeEventHandler;
  onChangeUseD: ChangeEventHandler;
}

const MCDUSelector = ({
  useM,
  useC,
  useD,
  onChangeUseM,
  onChangeUseC,
  onChangeUseD,
  ...props
}: MCDUSelectorProps) => (
  <div className={styles["mcdu-selector"]} {...props}>
    <label>
      <input type="checkbox" checked={useM} onChange={onChangeUseM} />
      <span>m</span>
    </label>
    <label>
      <input type="checkbox" checked={useC} onChange={onChangeUseC} />
      <span>c</span>
    </label>
    <label>
      <input type="checkbox" checked={useD} onChange={onChangeUseD} />
      <span>d</span>
    </label>
    <span>u</span>
  </div>
);

export { MCDUSelector };
