import { useEffect, ChangeEvent } from "react";

import { stateManager } from "./state-manager";
import { MCDUSelector } from "./mcdu-selector";
import { NumberSelector } from "./number-selector";
import { MCDUDrawing } from "./mcdu-drawing";
import { Spinner } from "./spinner";
import styles from "./styles.module.scss";

const App = () => {
  // state
  const [state, dispatch] = stateManager.useReducer();

  // init state manager and apply body style from css module
  useEffect(() => {
    stateManager.init(dispatch);
    document.body.classList.add(styles.body);
    return () => {
      document.body.classList.remove(styles.body);
    };
  }, []);

  // the number
  const number = stateManager.number(state);

  // build component
  return stateManager.ready(state) ? (
    <>
      <div className={styles["header"]}>
        <div className={styles["number-selector-container"]}>
          <NumberSelector
            value={stateManager.rawNumber(state)}
            showNumber={stateManager.showNumber(state)}
            wrongValue={number == null}
            onSetNumber={(e: ChangeEvent<HTMLInputElement>) =>
              dispatch({ type: "set-number", payload: e.target.value })
            }
            toggleShowNumber={(e: ChangeEvent<HTMLInputElement>) =>
              dispatch({
                type: "toggle-show-number",
                payload: e.target.checked,
              })
            }
          />
        </div>
        <MCDUSelector
          useM={stateManager.useM(state)}
          onChangeUseM={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "toggle-use-m",
              payload: e.target.checked,
            })
          }
          useC={stateManager.useC(state)}
          onChangeUseC={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "toggle-use-c",
              payload: e.target.checked,
            })
          }
          useD={stateManager.useD(state)}
          onChangeUseD={(e: ChangeEvent<HTMLInputElement>) =>
            dispatch({
              type: "toggle-use-d",
              payload: e.target.checked,
            })
          }
        />
      </div>
      {number != null ? (
        <MCDUDrawing
          number={number}
          useM={stateManager.useM(state)}
          useC={stateManager.useC(state)}
          useD={stateManager.useD(state)}
        />
      ) : undefined}
    </>
  ) : (
    <Spinner />
  );
};

export default App;
