const copy = (value: any): any => JSON.parse(JSON.stringify(value));

const localLoad = async (key: string, defaultValue: any): Promise<any> => {
  // prepare result
  const res = copy(defaultValue);
  // get local backup
  let local: any = {};
  try {
    const tmp = window.localStorage.getItem(key);
    if (tmp != null) local = JSON.parse(tmp);
  } catch (e) {}
  // read result properties from backup
  Object.keys(res).forEach((p: string) => {
    if (local.hasOwnProperty(p)) res[p] = local[p];
  });
  return res;
};

const localSave = async (key: string, value: any) => {
  try {
    window.localStorage.setItem(key, JSON.stringify(value));
  } catch (e) {}
};

export { localLoad, localSave, copy };
