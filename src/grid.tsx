import { useRef, useMemo, useEffect } from "react";
import { SVG } from "@svgdotjs/svg.js";

interface Grid23dProps extends React.HTMLAttributes<HTMLTableElement> {
  size: number[];
}

interface GridProps extends React.HTMLAttributes<HTMLTableElement> {
  size: number | number[];
}

const Grid2d = ({ size, ...props }: Grid23dProps) => (
  <table {...props}>
    <tbody>
      {[...Array(size[0])].map((_, key) => (
        <tr key={key}>
          {[...Array(size[1])].map((_, key) => (
            <td key={key} />
          ))}
        </tr>
      ))}
    </tbody>
  </table>
);

const Grid3d = ({ size, ...props }: Grid23dProps) => {
  const elemRef = useRef<HTMLDivElement>(null);
  const svg = useMemo(() => SVG(), []);

  useEffect(() => {
    if ((elemRef?.current?.children?.length ?? 2) < 1) {
      svg.addTo(elemRef?.current as HTMLDivElement);
    }
    svg.clear();

    const c = 13.7,
      p = 5;

    svg.size(size[1] * c + size[2] * p + 1, size[0] * c + size[2] * p + 1);
    svg.fill("transparent").stroke({ color: "currentColor", width: 1.5 });

    for (let i = 1; i <= size[0]; i++) {
      svg.polyline([
        [0, size[2] * p + i * c],
        [size[1] * c, size[2] * p + i * c],
        [size[1] * c + size[2] * p, i * c],
      ]);
    }

    for (let j = 0; j < size[1]; j++) {
      svg.polyline([
        [j * c, size[2] * p + size[0] * c],
        [j * c, size[2] * p],
        [j * c + size[2] * p, 0],
      ]);
    }

    for (let k = 0; k <= size[2]; k++) {
      svg.polyline([
        [(size[2] - k) * p, k * p],
        [(size[2] - k) * p + size[1] * c, k * p],
        [(size[2] - k) * p + size[1] * c, k * p + size[0] * c],
      ]);
    }

    svg.polyline([
      [size[1] * c, size[2] * p],
      [size[1] * c + size[2] * p, 0],
    ]);
  }, [size, elemRef, svg]);

  return <div ref={elemRef} {...props} />;
};

const Grid = ({ size, ...props }: GridProps) => {
  if (!Array.isArray(size)) size = [size, 1];
  else if (size.length == 1) size = [size[0], 1];

  if (size.length == 2) return <Grid2d size={size} {...props} />;
  else if (size.length == 3) return <Grid3d size={size} {...props} />;
  else throw new Error("invalid size");
};

export { Grid };
