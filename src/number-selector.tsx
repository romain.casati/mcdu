import styles from "./styles.module.scss";
import { ChangeEventHandler, useRef } from "react";

interface NumberSelectorProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  showNumber: boolean;
  wrongValue: boolean;
  onSetNumber: ChangeEventHandler;
  toggleShowNumber: ChangeEventHandler;
}

const NumberSelector = ({
  showNumber,
  wrongValue,
  onSetNumber,
  toggleShowNumber,
  ...props
}: NumberSelectorProps) => {
  const inputRef = useRef<HTMLInputElement>(null);

  return (
    <>
      <input
        type={showNumber ? "text" : "password"}
        ref={inputRef}
        className={styles["input-number"]}
        data-wrong-value={wrongValue}
        onChange={onSetNumber}
        {...props}
      />
      <label>
        <input
          type="checkbox"
          checked={showNumber}
          onChange={toggleShowNumber}
          className={styles["show-checkbox"]}
        />
        <span className={styles["eye"]} />
      </label>
    </>
  );
};

export { NumberSelector };
