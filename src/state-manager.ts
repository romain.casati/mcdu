import { type Dispatch } from "react";
/* we use immer to fake mutable state */
import { useImmerReducer } from "use-immer";
import { localLoad, localSave, copy } from "./local-load";

/* our global state type */
interface State {
  ready: boolean;
  number: string;
  showNumber: boolean;
  useM: boolean;
  useC: boolean;
  useD: boolean;
}

/* our dispatched action type */
interface Action {
  type:
    | "init"
    | "set-number"
    | "toggle-show-number"
    | "toggle-use-m"
    | "toggle-use-c"
    | "toggle-use-d";
  payload: string | boolean | State;
}

/* this initial state does not mean anithing until data is fetched */
const initialState: State = {
  ready: false,
  number: "314",
  showNumber: false,
  useM: true,
  useC: true,
  useD: true,
};

/* where our global logic takes place */
const reducer = (state: State, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case "init": {
      const _state = copy(payload) as State;
      _state.ready = true;
      return _state;
    }
    case "set-number":
      state.number = payload as string;
      break;
    case "toggle-show-number":
      state.showNumber = payload as boolean;
      break;
    case "toggle-use-m":
      state.useM = payload as boolean;
      break;
    case "toggle-use-c":
      state.useC = payload as boolean;
      break;
    case "toggle-use-d":
      state.useD = payload as boolean;
      break;
    default:
      throw Error(`Internal error: unrecognized action type: '${type}'!`);
  }
  localSave("state", state);
};

/* stateManager is mainly a wrapper arround review object */
const stateManager = {
  init: async (dispatch: Dispatch<Action>) =>
    dispatch({ type: "init", payload: await localLoad("state", initialState) }),
  ready: (state: State): boolean => state.ready,
  useReducer: () => useImmerReducer(reducer, initialState),
  rawNumber: (state: State): string => state.number,
  number: (state: State): number | undefined => {
    const number = Number(state.number);
    if (Number.isInteger(number) && number >= 0 && number < 100000)
      return number;
  },
  showNumber: (state: State): boolean => state.showNumber,
  useM: (state: State): boolean => state.useM,
  useC: (state: State): boolean => state.useC,
  useD: (state: State): boolean => state.useD,
};

export { stateManager };
